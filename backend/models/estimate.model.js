const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const estimateSchema = new Schema({
  value: {
    type: Number,
  },
});

const estimate = mongoose.model('Estimates', estimateSchema);

module.exports = estimate;