const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const estimateSchema = new Schema({
  _userId: {
    type: Schema.Types.ObjectId,
    unique: true,
    auto: true,
    index: true,
    sparse:true,
  },
  estimate: {
    type: Number,
  }
})

const taksSchema = new Schema({
  description: {
    type: String,
    required: true,
    minlength: 3,
  },
  status: {
    type: Number,
    default: 0,
  },
  estimates: [estimateSchema]
}, {
  timestamps: true,
});

const Task = mongoose.model('Tasks', taksSchema);

module.exports = Task;