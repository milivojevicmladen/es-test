const router = require('express').Router();
let Estimate = require('../models/estimate.model');

router.route('/read').get((req, res) => {
  Estimate.find()
    .then(estimates => res.json(estimates))
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;