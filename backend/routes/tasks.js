const router = require('express').Router();
let Task = require('../models/task.model');

// CREATE

router.route('/create').post((req, res) => {
  const task = req.body;
  const newTask = new Task(task);

  newTask.save()
    .then(() => res.json(newTask))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/estimates/create').post((req, res) => {
  const { taskId, estimate } = req.body;
  
  if (taskId) {
    Task.findById(taskId)
    .then(task => {
      task.estimates.push({ estimate: estimate })
      task.save()
        .then(() => res.json({ task: task, estimate: task.estimates[task.estimates.length - 1] }))
        .catch(err => res.status(400).json('Error: ' + err));
    })
    .catch(err => res.status(400).json('Error: ' + err));    
  }
});

// READ

router.route('/read').get((req, res) => {
  Task.find()
    .then(tasks => res.json(tasks))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/read/:taskId').get((req, res) => {
  const { taskId } = req.params

  if (taskId) {
    Task.findById(taskId)
    .then(task => res.json(task))
    .catch(err => res.status(400).json('Error: ' + err));    
  }
});

router.route('/read/estimates').get((req, res) => {
  const { taskId } = req.body
  Task.findById(taskId)
    .then(task => res.json(task.estimates))
    .catch(err => res.status(400).json('Error: ' + err));
});

// UPDATE

router.route('/update').post((req, res) => {
  const { taskId, userId, estimate } = req.body
  
  if (taskId && userId) {
    Task.findById(taskId)
      .then(task => {
        if (estimate) {
          for (const _estimate of task.estimates) {
            if (_estimate._userId.toString() === userId) {
              _estimate.set({ estimate });
            }
          }
        }
        else {
          task.estimates = task.estimates.filter(_estimate => _estimate._userId.toString() !== userId)
        }
  
        task.save()
          .then(() => res.json(task))
          .catch(err => res.status(400).json('Error: ' + err));
      })
      .catch(err => res.status(400).json('Error: ' + err));
  }
});

// DELETE

router.route('/delete').delete((req, res) => {
  const { taskId } = req.body
  
  if (taskId) {
    Task.findByIdAndRemove(taskId, function(error) {
      res.json(taskId)
      if (error) {
        res.status(400).json('Error: ' + error)
      }
    })
  }
});

module.exports = router;