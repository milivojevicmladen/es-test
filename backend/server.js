  
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const dotenv = require('dotenv')
const app = express();
const socketIo = require('socket.io');
const http = require('http')
dotenv.config();

const port = process.env.PORT || 5000;
const server = http.createServer(app)
const io = socketIo(server)

const tasksRouter = require('./routes/tasks');
const estimatesRouter = require('./routes/estimates');

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true });
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established successfully");
});

app.use('/tasks', tasksRouter);
app.use('/estimates', estimatesRouter);

io.on('connection', (socket) => {
    socket.on('cast-vote', (data) => {
        io.sockets.emit('data-updated', data)
    })
    socket.on('disconnect', () => console.log("Client disconnected"));
});

server.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});