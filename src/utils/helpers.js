export const copyTextToClipboard = (text) => {
  if (text) {
    const el = document.createElement('textarea')
    el.value = text
    el.style = { display: 'none' }
    document.body.appendChild(el)
    el.select()
    el.focus()
    document.execCommand('copy')
    document.body.removeChild(el)
  }
}

export const getSummarizedResults = (estimates) => {
  function getLowestValue() {
    return Math.min(...estimates)
  }

  function getAverageValue() {
    let sum = 0
    for (const estimate of estimates) {
      sum += estimate
    }
    return (sum/estimates.length).toFixed(1)
  }

  function getHighestValue() {
    return Math.max(...estimates)
  }

  return [
    { label: 'Lowest', value: getLowestValue() },
    { label: 'Average', value: getAverageValue() },
    { label: 'Highest', value: getHighestValue() },
  ]
}
