import type { Dispatch } from 'redux'
import { addItems, addItem } from './dataActions'
import Api from '../api'

export const loadTasks = () => {
  return async (dispatch: Dispatch<*>) => {
    const result = await Api.fetchTasks()
    const allTasks = result.data
    dispatch(addItems(allTasks))
    return allTasks
  }
}

export const loadTask = (taskId) => {
  return async (dispatch: Dispatch<*>) => {
    const result = await Api.fetchTask(taskId)
    const task = result.data
    dispatch(addItem(task))
    return task
  }
}