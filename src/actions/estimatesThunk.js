import type { Dispatch } from 'redux'
import { addItems } from './estimatesActions'
import Api from '../api'

export const loadEstimates = () => {
  return async (dispatch: Dispatch<*>) => {
    const result = await Api.fetchEstimates()
    const estimates = result.data
    dispatch(addItems(estimates))
    return estimates
  }
}