import {
  ADD_ESTIMATE_ITEM,
  ADD_ESTIMATE_ITEMS,
  RESET_ESTIMATE_STATE,
} from '../reducers/estimatesReducer'

export const addItem = (item) => {
  return {
    type: ADD_ESTIMATE_ITEM,
    item,
  }
}

export const addItems = (items) => {
  return {
    type: ADD_ESTIMATE_ITEMS,
    items,
  }
}

export const resetState = () => {
  return {
    type: RESET_ESTIMATE_STATE,
  }
}
