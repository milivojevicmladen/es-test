export const rootPath = () => `/`
export const taskEstimationPath = (taskId) => `/task/${taskId}`
export const baseUrl = () => 'http://localhost:3000'