import indigo from '@material-ui/core/colors/indigo'

export default {
  indigoSuperLight: indigo[50],
  indigoLight: indigo[400],
  indigo: indigo[500],
  indigoDark: indigo[600]
}