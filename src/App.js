import React from 'react';
import { Provider } from 'react-redux'
import { initStore } from './store'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Routes from './routes'

function App() {
  const store = initStore()

  return (
    <Provider store={store}>
      <Router>
        <Switch>
          { Routes.map(({ name, ...routeProps }) => <Route key={`route-${name}`} {...routeProps} />) }
        </Switch>
      </Router>
    </Provider>
  )
}

export default App
