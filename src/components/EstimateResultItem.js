import React from 'react'
import { Tooltip, IconButton, Avatar, ListItem, makeStyles, Typography } from '@material-ui/core'
import PersonIcon from '@material-ui/icons/Person'
import ClearIcon from '@material-ui/icons/Clear'

const useStyles = makeStyles(theme => ({
  listItem: {
    marginBottom: theme.spacing(1),
    backgroundColor: theme.palette.grey[200],
    borderRadius: 6,
    display: 'flex',
    justifyContent: 'space-between',
  },
  userWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    marginRight: theme.spacing(1),
  },
  estimate: {
    fontWeight: theme.typography.fontWeightBold,
    marginLeft: theme.spacing(1)
  }
}))

const EstimateResultItem = (props) => {
  const { estimate, selectedEstimate, onWithdrawVote } = props
  const classes = useStyles(props)
  const isSelected = selectedEstimate && selectedEstimate._id === estimate._id
  const userText = isSelected ? 'You' : 'User'

  function withdrawVote(event) {
    onWithdrawVote && selectedEstimate && onWithdrawVote(event, selectedEstimate)
  }

  return (
    <ListItem classes={{ root: classes.listItem }}>
      <div className={classes.userWrapper}>
        <Avatar classes={{ root: classes.avatar }}><PersonIcon /></Avatar>
        <Typography variant={'body1'}>{userText} voted for:</Typography>
        <Typography variant={'body1'} classes={{ root: classes.estimate }}>{estimate.estimate}</Typography>
      </div>
      { 
        isSelected && (
          <Tooltip title={'Withdraw vote'} placement="top">
            <IconButton
                aria-label="remove"
                aria-haspopup="true"
                onClick={withdrawVote}
                size={'small'}
                classes={{ root: classes.closeButton }}
              >
                <ClearIcon />
            </IconButton>
          </Tooltip>
        )
      }
    </ListItem>
  )
}

export default EstimateResultItem