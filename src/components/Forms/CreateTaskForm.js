import React, { useState } from 'react'
import { TextField, Button, makeStyles } from '@material-ui/core'
import Colors from '../../constants/colors'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
  },
  submitButton: {
    marginTop: theme.spacing(4),
    backgroundColor: Colors.indigo,
    color: theme.palette.common.white,
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
  }
}))

const CreateTaskForm = (props) => {
  const { onSubmit } = props
  const classes = useStyles(props)
  const [ textFieldValue, setTextFieldValue ] = useState(null)

  function handleTextFieldChange(event) {
    const { value } = event.target
    setTextFieldValue(value)
  }

  function handleSubmit(event) {
    if (textFieldValue && textFieldValue.length > 3) {
      setTextFieldValue(null)
      onSubmit(textFieldValue)
    }
  }

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField
        label={'Task description'}
        placeholder={'What do you need to accomplish?'}
        onChange={handleTextFieldChange}
        fullWidth
        size={'medium'}
        variant={'outlined'}
        required
        autoFocus
      />
      <Button
        classes={{ root: classes.submitButton }}
        disabled={!textFieldValue || textFieldValue.length < 3} 
        onClick={handleSubmit} 
        variant="contained"
      >
        Create task
      </Button>
    </form>
  )
}

export default CreateTaskForm