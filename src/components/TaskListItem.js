import React from 'react'
import { ListItem, ListItemAvatar, ListItemText, ListItemSecondaryAction, IconButton, Avatar, Menu, MenuItem, makeStyles, Typography } from '@material-ui/core'
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn'
import DeleteIcon  from '@material-ui/icons/Delete'
import MoreVertIcon  from '@material-ui/icons/MoreVert'
import ShareIcon from '@material-ui/icons/Share'
import { taskEstimationPath } from '../constants/paths'
import { Link } from 'react-router-dom'
import { useMenuState } from '../hooks/mui-hooks'
const ITEM_HEIGHT = 48

const useStyles = makeStyles(theme => ({
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  },
  menuItem: {
    minHeight: 50,
  },
  menuIcon: {
    color: theme.palette.grey[800],
    marginRight: theme.spacing(2)
  },
  listItem: {
    marginBottom: theme.spacing(1),
    borderRadius: 6,
  }
}))

const TaskListItem = (props) => {
  const { task, onDelete, onShared } = props
  const path = taskEstimationPath(task._id)
  const classes = useStyles(props)
  const [taskListItemAnchor, openTaskListItemMenu, closeTaskListItemMenu] = useMenuState(null)

  function onTaskDelete(event) {
    onDelete && onDelete(event, task)
    closeTaskListItemMenu()
  }

  function onTaskShared(event) {
    onShared && onShared(event, path)
    closeTaskListItemMenu()
  }

  function openMenu(event) {
    event.stopPropagation()
    event.preventDefault()
    openTaskListItemMenu(event)
  }

  return (
    <>
      <Link to={path} className={classes.link}>
        <ListItem button classes={{root: classes.listItem }}>
          <ListItemAvatar>
            <Avatar>
              <AssignmentTurnedInIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={task.description}/>
          <ListItemSecondaryAction>
            <IconButton
              aria-label="more"
              aria-haspopup="true"
              onClick={openMenu}
            >
              <MoreVertIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </Link>
      {
        taskListItemAnchor && (
          <Menu
            anchorEl={taskListItemAnchor}
            keepMounted
            open={Boolean(taskListItemAnchor)}
            onClose={closeTaskListItemMenu}
            PaperProps={{
              style: {
                maxHeight: ITEM_HEIGHT * 4.5,
                width: 200,
              },
            }}
          >
            <MenuItem classes={{ root: classes.menuItem }} onClick={onTaskShared}>
              <ShareIcon classes={{ root: classes.menuIcon }} />
              <Typography>Copy task link</Typography>
            </MenuItem>
            <MenuItem classes={{ root: classes.menuItem }} onClick={onTaskDelete}>
              <DeleteIcon classes={{ root: classes.menuIcon }} />
              <Typography>Delete task</Typography>
            </MenuItem>
          </Menu>
        )
      }
    </>
  )
}

export default TaskListItem