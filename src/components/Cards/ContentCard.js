import React from 'react'
import { Card, CardContent, Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  card: {
    padding: theme.spacing(3),
    borderTopLeftRadius: theme.spacing(2),
    borderTopRightRadius: theme.spacing(2),
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    height: '100%',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
  },
  children: {
    height: '55vh',
    overflowY: 'auto',
    marginTop: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }
}))

const ContentCard = (props) => {
  const { title, additionalButton, children } = props
  const classes = useStyles(props)
  
  return (
    <div className={classes.root}>
      <Card classes={{ root: classes.card }}>
        <CardContent classes={{ root: classes.cardContent }}>
          <div className={classes.header}>
            <Typography variant={'h5'}>
              { title }
            </Typography>
            { additionalButton && additionalButton }
          </div>
          <div className={classes.children}>
            { children }
          </div>
        </CardContent>
      </Card>
    </div>
  )
}

export default ContentCard