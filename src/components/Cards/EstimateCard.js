import React from 'react'
import { Card, CardContent, Typography, makeStyles } from '@material-ui/core'
import classNames from 'classnames'
import Colors from '../../constants/colors'

const useStyles = makeStyles(theme => ({
  card: {
    marginRight: theme.spacing(3),
    marginBottom: theme.spacing(3),
    borderRadius: 12,
    backgroundColor: theme.palette.grey[50],
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: Colors.indigoSuperLight,
      color: theme.palette.common.white,
    }
  },
  selectedCard: {
    backgroundColor: Colors.indigoSuperLight,
  },
  disabledCard: {
    opacity: 0.5,
    pointerEvents: 'none',
  },
  cardContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 180,
  },
  value: {
    color: Colors.indigoLight,
    fontWeight: theme.typography.fontWeightBold,
  },
}))

const EstimateCard = (props) => {
  const { estimateOption, isSelected, selectedEstimate, onCardClick } = props
  const classes = useStyles(props)
  const disabled = Boolean(selectedEstimate)

  function onClick(event) {
    onCardClick && !disabled && onCardClick(event, estimateOption)
  }

  return (
    <Card 
      classes={{ root: classNames(classes.card, disabled && classes.disabledCard, isSelected && classes.selectedCard) }} 
      elevation={isSelected ? 3 : 1} 
      onClick={onClick}
    >
      <CardContent classes={{ root: classes.cardContent }}>
        <Typography variant={'h3'} classes={{ root: classes.value }}>
          { estimateOption.value }
        </Typography>
      </CardContent>
    </Card>
  )
}

export default EstimateCard