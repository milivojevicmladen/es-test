import React, { useState } from 'react'
import EstimateCard from './EstimateCard'
import { Grid } from '@material-ui/core'  

const EstimateCards = (props) => {
  const { estimates, selectedEstimate, onEstimate } = props
  const [selectedCard, setSelectedCard] = useState(null)

  function onCardClick(event, estimateOption) {
    setSelectedCard(estimateOption)
    onEstimate && onEstimate(event, estimateOption)
  }

  return (
    <Grid container justify={'center'}>
      {
        estimates.map((estimateOption) => (
          <Grid key={`estimate-card-${estimateOption._id}`} item xs={12} sm={12} md={3} lg={3} xl={3}>
            <EstimateCard
              selectedEstimate={selectedEstimate}
              estimateOption={estimateOption}
              onCardClick={onCardClick}
              isSelected={selectedCard && selectedEstimate && selectedCard._id === estimateOption._id}
            />
          </Grid>
        ))
      }
    </Grid>
  )
}

export default EstimateCards