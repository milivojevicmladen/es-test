import React from 'react'
import { Drawer, IconButton, List, Typography, makeStyles } from '@material-ui/core'
import ListEmptyState from '../EmptyStates/ListEmptyState'
import InboxIcon from '@material-ui/icons/Inbox'
import ResultSummary from '../ResultSummary'
import CloseIcon from '@material-ui/icons/Close'
import EstimateResultItem from '../EstimateResultItem'

const useStyles = makeStyles(theme => ({
  drawerHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    position: 'relative',
  },
  drawerPaper: {
    width: 350,
    flexShrink: 0,
    padding: theme.spacing(4)
  },
  estimatesWrapper: {
    overflowY: 'auto',
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4)
  },
  closeButton: {
    position: 'absolute',
    top: -8,
    right: 0,
  },
}))

const VotingResultsDrawer = (props) => {
  const { estimates, drawerAnchorEl, closeDrawer, onWithdraw, selectedEstimate } = props
  const classes = useStyles()

  function withdrawVote(event, estimateOption) {
    onWithdraw && onWithdraw(event, estimateOption)
  }

  return (
    <Drawer
      variant="persistent"
      open={drawerAnchorEl}
      onClose={closeDrawer}
      classes={{ paper: classes.drawerPaper }}
      anchor="right"
    >
      <div className={classes.drawerHeader}>
        <Typography variant={'h5'} className={classes.title} gutterBottom>Voting Results</Typography>
        <IconButton
          aria-label="close"
          aria-haspopup="true"
          onClick={closeDrawer}
          classes={{ root: classes.closeButton }}
        >
          <CloseIcon />
        </IconButton>
      </div>
      { estimates && estimates.length > 0 && <ResultSummary estimates={estimates} /> }
      <div className={classes.estimatesWrapper}>
        { estimates.length === 0 && <ListEmptyState icon={<InboxIcon />} title={'Voting has not started yet'} description={'Invite colleagues to start voting'} /> }
        { 
          estimates.length > 0 && (
            <>
              <Typography variant={'subtitle1'} className={classes.title}>Participant's votes:</Typography>
              <List>
                {
                  estimates.map((estimate, index) => (
                    <EstimateResultItem key={`estimate-item-${estimate._id}`} selectedEstimate={selectedEstimate} estimate={estimate} onWithdrawVote={withdrawVote} />
                  ))
                }
              </List>
            </>
          )
        }
      </div>
    </Drawer>
  )
}

export default VotingResultsDrawer
