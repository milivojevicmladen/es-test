import React from 'react'
import { Card, CardContent, Typography, makeStyles } from '@material-ui/core'
import { getSummarizedResults } from '../utils/helpers'
import Colors from '../constants/colors'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5)
  },
  summarizedResults: {
    display: 'flex',
    justifyContent: 'stretch'
  },
  resultWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    marginRight: theme.spacing(1),
  },
  card: {
    backgroundColor: theme.palette.grey[200],
    border: 'none',
    width: '100%',
    marginBottom: theme.spacing(1),
  },
  cardContent: {
    display: 'flex',
    justifyContent: 'center',
    paddingBottom: theme.spacing(2) + 'px !important'
  },
  numberOfParticipantsWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  resultValue: {
    color: Colors.indigo,
    fontWeight: theme.typography.fontWeightBold,
  }
}))

const ResultSummary = (props) => {
  const { estimates } = props
  const classes = useStyles(props)

  const users = [ ...new Set(estimates.map(estimate => estimate._userId)) ]
  const estimatedValues = [ ...new Set(estimates.map(estimate => estimate.estimate)) ]
  const numberOfUsers = users.length
  const results = getSummarizedResults(estimatedValues)

  return (
    <div className={classes.root}>
      <div className={classes.numberOfParticipantsWrapper}>
        <Typography variant={'body2'}>Number of participants:</Typography>
        <Typography variant={'body2'}>{numberOfUsers}</Typography>
      </div>
      <div className={classes.summarizedResults}>
        {
          results.map((result, index) => (
            <div key={`result-summary-${index}`} className={classes.resultWrapper}>
              <Card classes={{ root: classes.card }} variant="outlined">
                <CardContent classes={{ root: classes.cardContent }}>
                  <Typography variant={'h5'} classes={{ root: classes.resultValue }}>{result.value}</Typography>
                </CardContent>
              </Card>
              <Typography variant={'subtitle2'} >{result.label}</Typography>
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default ResultSummary