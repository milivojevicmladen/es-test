import React from 'react'
import { List, makeStyles } from '@material-ui/core'
import TaskListItem from './TaskListItem'

const useStyles = makeStyles({
  list: {
    width: '100%',
  },
})

const TaskList = (props) => {
  const { tasks, onTaskShared, onTaskDeleted } = props
  const classes = useStyles()

  return (
    <List classes={{ root: classes.list }}>
      { tasks.map(task => <TaskListItem key={`task-item-${task._id}`} task={task} onDelete={onTaskDeleted} onShared={onTaskShared} />) }
    </List>
  )
}

export default TaskList