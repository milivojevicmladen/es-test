import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(12),
    paddingLeft: theme.spacing(6),
    paddingRight: theme.spacing(6),
  },
  circle: {
    height: 86,
    width: 86,
    borderRadius: '100%',
    backgroundColor: theme.palette.grey[100],

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    fontSize: theme.typography.h2.fontSize,
    color: theme.palette.grey[300],
  },
  titleWrapper: {
    marginTop: theme.spacing(1),
    textAlign: 'center',
  },
  title: {
    fontSize: theme.typography.h5.fontSize,
    color: theme.palette.text.primary,
    marginBottom: theme.spacing(1),
  },
  description: {
    fontSize: theme.typography.body1.fontSize,
    color: theme.palette.text.secondary,
  },
}))

const ListEmptyState = (props) => {
  const { icon, title, description } = props
  const classes = useStyles(props)

  return (
    <div className={classes.root}>
      <div className={classes.circle}>
        {icon}
      </div>
      <div className={classes.titleWrapper}>
        <>
          <div className={classes.title}>{title}</div>
          <div className={classes.description}>{description}</div>
        </>
      </div>
    </div>
  )
}

export default ListEmptyState
