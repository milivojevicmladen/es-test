import React from 'react'
import { Slide } from '@material-ui/core'

const TransitionUp = (props) => {
  return <Slide {...props} direction="up" />
}

export default TransitionUp