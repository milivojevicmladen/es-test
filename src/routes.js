import React from 'react'
import TasksScene from './scenes/TasksScene'
import TaskEstimationScene from './scenes/TaskEstimationScene'

const Routes = [
  {
    name: 'taskEstimation',
    path: '/task/:taskId',
    exact: true,
    component: TaskEstimationScene
  },
  {
    path: '/',
    name: 'tasks',
    component: TasksScene,
  },
]

export default Routes
