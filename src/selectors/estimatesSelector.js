import { createSelectorWithDependencies as createSelector } from 'reselect-tools'

export const estimates = state => state.estimates

export const getEstimates = createSelector(
  [estimates],
  (x) => Object.values(x),
)