import { createSelectorWithDependencies as createSelector } from 'reselect-tools'

export const tasks = state => state.data
export const getTask = (state, taskId): Item => state.data[taskId]

export const getTasks = createSelector(
  [tasks],
  (x) => Object.values(x),
)

export const getTasksCount = createSelector(
  [tasks],
  (x) => Object.values(x).length,
)