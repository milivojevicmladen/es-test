import * as data from './dataSelector'
import * as estimates from './estimatesSelector'

export default {
  ...data,
  ...estimates,
}
