export default {
  createTask: 'tasks/create',
  createEstimate: 'tasks/estimates/create',
  readTask: (taskId) => `tasks/read/${taskId}`,
  readTasks: 'tasks/read',
  updateTask: 'tasks/update',
  deleteTask: 'tasks/delete',

  readEstimates: 'estimates/read',
}