import endpoints from './endpoints'
import { get } from './requestWrappers'

export const fetchTasks = async () => {
  return await get(endpoints.readTasks)
}

export const fetchTask = async (taskId) => {
  return await get(endpoints.readTask(taskId))
}

export const fetchEstimates = async () => {
  return await get(endpoints.readEstimates)
}