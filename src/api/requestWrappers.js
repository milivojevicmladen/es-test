import axios from 'axios'
import { getBaseUrl } from './index'

const request = (method, url, payload, headers, options: { overrideData: boolean } = {}) => {

  return axios(
    {
      method,
      url: `${getBaseUrl()}${url}`,
      data: payload,
      crossdomain : true,
      headers: {
        'Content-Type': 'application/json;  charset=UTF-8',
        'Access-Control-Allow-Origin': '*',
        ...headers,
      },
    },
  )
}

export const get = (url, headers) => request('GET', url, null, headers)
export const deleteRequest = (url, payload, headers) => request('DELETE', url, payload, headers)
export const post = (url, payload, headers, options) => request('POST', url, payload, headers, options)
export const update = (url, payload, headers, options) => request('UPDATE', url, payload, headers, options)

