import endpoints from './endpoints'
import { get, deleteRequest, post, update } from './requestWrappers'
import { fetchTask, fetchTasks, fetchEstimates } from './fetchFunctions'

export const getBaseUrl = () => 'http://localhost:5000/'

export default { 
  endpoints,
  get,
  deleteRequest,
  post,
  update,
  fetchTask,
  fetchTasks,
  fetchEstimates,
}
