import data from './dataReducer'
import estimates from './estimatesReducer'

export default {
  data,
  estimates
}
