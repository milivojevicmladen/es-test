export const ADD_ESTIMATE_ITEM = 'ADD_ESTIMATE_ITEM'
export const ADD_ESTIMATE_ITEMS = 'ADD_ESTIMATE_ITEMS'
export const RESET_ESTIMATE_STATE = 'RESET_ESTIMATE_STATE'

type State = {
  +[string]: Item,
}

const initialState: State = {}

export default (state: State = initialState, action: Object): State => {
  switch (action.type) {
    case ADD_ESTIMATE_ITEM:
      return {
        ...state,
        [action._id]: action.item,
      }

    case ADD_ESTIMATE_ITEMS: {
      return {
        ...state,
        ...action.items
          .reduce((acc, item) => ({
            ...acc,
            [item._id]: item,
          }), {}),
      }
    }

    case RESET_ESTIMATE_STATE:
      return {
        ...initialState,
      }

    default:
      return state
  }
}
