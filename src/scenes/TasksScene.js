import React, { useEffect, useState } from 'react'

import { loadTasks } from '../actions/dataThunk'
import { getTasks } from '../selectors/dataSelector'
import { addItem } from '../actions/dataActions'
import { removeItem } from '../actions/dataActions'
import { useDispatch, useSelector } from 'react-redux'
import Api from '../api'

import { baseUrl } from '../constants/paths'
import { copyTextToClipboard } from '../utils/helpers'

import ContentCard from '../components/Cards/ContentCard'
import CreateTaskForm from '../components/Forms/CreateTaskForm'
import { CircularProgress, Grid, Typography, Snackbar, makeStyles } from '@material-ui/core'
import TaskList from '../components/TaskList'

import ListEmptyState from '../components/EmptyStates/ListEmptyState'
import AssignmentIcon from '@material-ui/icons/Assignment'
import TransitionUp from '../components/TransitionUp'

const useStyles = makeStyles(theme => ({
  formWrapper: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    display: 'flex',
    justifyContent: 'center',
  },
  title: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
  },
  gridItem: {
    paddingBottom: '0 !important',
  },
  pageHeader: {
    paddingTop: theme.spacing(10),
    marginBottom: theme.spacing(10)
  }
}))

const TasksScene = (props) => {
  const dispatch = useDispatch()
  const tasks = useSelector(getTasks)
  const classes = useStyles(props)
  
  const [dataLoading, setDataLoading] = useState(true)
  const [snackbarMessage, setSnackbarMessage] = useState(null)
  const [snackbarOpen, setSnackbarOpen] = useState(null)

  useEffect(() => {
    setDataLoading(true)

    try {
      dispatch(loadTasks())
    } catch (error) {
      console.error('Could not get tasks ', error)
    } finally {
      setDataLoading(false)
    }
  }, [])

  async function onFormSubmitted(value) {
    try {
      const result = await Api.post(Api.endpoints.createTask, { "description": value })
      dispatch(addItem(result.data))
    } catch (error) {
      console.error('Could not create a new task ', error)
    } finally {
      openSnackbar(`New task created...`)
    }
  }

  async function onTaskDelete(event, task) {
    try {
      const result = await Api.deleteRequest(Api.endpoints.deleteTask, { "taskId": task._id })
      dispatch(removeItem(result.data))
    } catch (error) {
      console.error('Could not create a new task ', error)
    } finally {
      openSnackbar(`${task.description} deleted`)
    }
  }

  function onTaskShared(event, path) {
    const url = baseUrl() + path
    copyTextToClipboard(url)
    openSnackbar('Link copied to clipboard')
  }

  function openSnackbar(message) {
    setSnackbarMessage(message)
    setSnackbarOpen(true)
  }

  function closeSnackbar() {
    setSnackbarOpen(false)
  }

  return (
    <Grid container justify={'center'}>
      <Grid classes={{ root: classes.gridItem }} item xs={12} sm={10} md={8} lg={6} xl={6}>
        <div className={classes.pageHeader}>
          <Typography variant={'h4'} className={classes.title} gutterBottom>
            Create a task to estimate
          </Typography>
          <div className={classes.formWrapper}>
            <CreateTaskForm onSubmit={onFormSubmitted} />
          </div>
        </div>
        <ContentCard title={'List of tasks'}>
          { dataLoading && <CircularProgress /> }

          { 
            !dataLoading && tasks.length === 0 && (
              <ListEmptyState 
                icon={<AssignmentIcon />} 
                title={'Task List empty'} 
                description={'Please create a task using the form above'} 
              />
            )
          }
          
          { tasks && tasks.length > 0 && <TaskList tasks={tasks} onTaskShared={onTaskShared} onTaskDeleted={onTaskDelete} /> }
        </ContentCard>
        {
          snackbarOpen && (
            <Snackbar
              open={snackbarOpen}
              onClose={closeSnackbar}
              TransitionComponent={TransitionUp}
              message={snackbarMessage}
              autoHideDuration={3000}
            />
          )
        }
      </Grid>
    </Grid>
  )
}

export default TasksScene