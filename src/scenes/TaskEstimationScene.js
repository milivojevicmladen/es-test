import React, { useEffect, useState } from 'react'

import { addItem } from '../actions/dataActions'
import { loadTask } from '../actions/dataThunk'
import { loadEstimates } from '../actions/estimatesThunk'
import { getTask } from '../selectors/dataSelector'
import { getEstimates } from '../selectors/estimatesSelector'
import { useDispatch, useSelector } from 'react-redux'
import { rootPath } from '../constants/paths'

import { CircularProgress, Button, Grid, Typography, AppBar, Toolbar, Fab, Snackbar, makeStyles } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Link } from 'react-router-dom'
import EstimateCards from '../components/Cards/EstimateCards'
import VotingResultsDrawer from '../components/Drawers/VotingResultsDrawer'
import Api from '../api'
import ContentCard from '../components/Cards/ContentCard'
import ShareIcon from '@material-ui/icons/Share'
import Colors from '../constants/colors'
import { baseUrl } from '../constants/paths'
import { taskEstimationPath } from '../constants/paths'
import { copyTextToClipboard } from '../utils/helpers'
import TransitionUp from '../components/TransitionUp'

import io from 'socket.io-client'
import { getBaseUrl } from '../api'

const useStyles = makeStyles(theme => ({
  appBar: {
    backgroundColor: 'transparent',
    border: 'none',
    boxShadow: 'initial',
    paddingTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  toolbar: {
    padding: 0,
    display: 'flex',
    justifyContent: 'space-between',
  },
  gridItem: {
    paddingBottom: '0 !important',
  },
  pageHeader: {
    paddingTop: theme.spacing(9),
    marginBottom: theme.spacing(12),
    textAlign: 'center',
  },
  shareButton: {
    backgroundColor: Colors.indigo,
    color: theme.palette.common.white,
    '&:hover': {
      backgroundColor: Colors.indigoDark,
    }
  },
  estimateCardsWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }
}))

const TaskEstimationScene = (props) => {
  const { match } = props
  const { taskId } = match.params
  const classes = useStyles()
  const dispatch = useDispatch()
  const socket = io.connect(getBaseUrl())

  const [tasksLoading, setTasksLoading] = useState(true)
  const [estimatesLoading, setEstimatesLoading] = useState(true)
  const [dataChanged, setDataChanged] = useState(false)
  const [selectedEstimate, setSelectedEstimate] = useState(null)
  const [snackbarMessage, setSnackbarMessage] = useState(null)
  const [snackbarOpen, setSnackbarOpen] = useState(null)
  const [drawerOpen, setDrawerOpen] = useState(true)

  const task = useSelector(state => getTask(state, taskId))
  const estimates = useSelector(getEstimates)

  socket.on('data-updated', (data) => {
    setDataChanged(!dataChanged)
  })

  useEffect(() => {
    setTasksLoading(true)
    try {
      dispatch(loadTask(taskId))
    } catch (error) {
      console.error('Could not get tasks ', error)
    } finally {
      setTasksLoading(false)
    }
  }, [dataChanged])

  useEffect(() => {
    setEstimatesLoading(true)
    try {
      dispatch(loadEstimates())
    } catch (error) {
      console.error('Could not get estimates ', error)
    } finally {
      setEstimatesLoading(false)
    }
  }, [])

  async function onEstimate(event, estimateOption) {
    try {
      const result = await Api.post(Api.endpoints.createEstimate, { taskId, estimate: estimateOption.value })
      setSelectedEstimate(result.data.estimate)
      dispatch(addItem(result.data.task))
      socket.emit('cast-vote', result.data.task)
    } catch (error) {
      console.error('Could not create an estimate ', error)
    } finally {
      openDrawer(event)
      openSnackbar(`You voted with ${estimateOption.value}`)
    }
  }

  async function onWithdraw(event, estimateOption) {
    try {
      const result = await Api.post(Api.endpoints.updateTask, { taskId, userId: estimateOption._userId })
      dispatch(addItem(result.data))
      setSelectedEstimate(null)
      socket.emit('cast-vote', result.data)
    } catch (error) {
      console.error('Could not create an estimate ', error)
    } finally {
      openSnackbar(`Vote ${estimateOption.estimate} withdrawn. You can vote again.`)
    }
  }

  function openSnackbar(message) {
    setSnackbarMessage(message)
    setSnackbarOpen(true)
  }

  function closeSnackbar() {
    setSnackbarOpen(false)
  }
  
  function openDrawer() {
    setDrawerOpen(true)
  }

  function closeDrawer() {
    setDrawerOpen(false)
  }

  function shareButtonClicked(event) {
    const url = baseUrl() + taskEstimationPath(task._id)
    copyTextToClipboard(url)
    openSnackbar('Link copied to clipboard')
  }

  if (!task) {
    return null
  }
  
  return (
    <>
      <Grid container justify={'center'}>
        <Grid classes={{ root: classes.gridItem }} item xs={12} sm={10} md={8} lg={6} xl={6}>
          <AppBar classes={{ root: classes.appBar }} position={'relative'}>
            <Toolbar classes={{ root: classes.toolbar }}>
              <Link to={rootPath}><ArrowBackIcon /></Link>
              <Fab size={'medium'} aria-label="share" className={classes.shareButton} onClick={shareButtonClicked}>
                <ShareIcon />
              </Fab>
            </Toolbar>
          </AppBar>
          <div className={classes.pageHeader}>
            <Typography variant={'h5'} className={classes.title} gutterBottom>
              { task.description }
            </Typography>
          </div>
          <ContentCard title={'Your vote:'} additionalButton={<Button onClick={openDrawer}>Show results</Button>}>
            <div className={classes.estimateCardsWrapper}>
              { (tasksLoading || estimatesLoading) && <CircularProgress /> }
              { !estimatesLoading && estimates && <EstimateCards estimates={estimates} onEstimate={onEstimate} selectedEstimate={selectedEstimate} /> }
            </div>
          </ContentCard>
        </Grid>
      </Grid>
      {
        drawerOpen && (
          <VotingResultsDrawer 
            drawerAnchorEl={drawerOpen} 
            closeDrawer={closeDrawer}
            estimates={task.estimates}
            onWithdraw={onWithdraw}
            selectedEstimate={selectedEstimate}
          />
        )
      }
      {
        snackbarOpen && (
          <Snackbar
            open={snackbarOpen}
            onClose={closeSnackbar}
            TransitionComponent={TransitionUp}
            message={snackbarMessage}
          />
        )
      }
    </>
  )
}

export default TaskEstimationScene