## Planning Poker Test (Epidemic Sound)

##### Screenshots:

![Screenshot 1](https://i.ibb.co/k0BWzRB/Screenshot-2020-02-06-at-23-39-15.png)

![Screenshot 2](https://i.ibb.co/W5kmVyt/Screenshot-2020-02-06-at-23-39-36.png)

##### Steps to run the Application:

Clone the project to your local machine

#### Server

1. Navigate to `es-test/backend` folder
2. Run `npm install`
3. After installation is complete, run `npm start`
4. The server should automatically run in the terminal.
5. Keep the terminal window running, as it serves the application

#### Client Application

1. Open another terminal window
2. Navigate back to the project's root folder `/es-test`
3. Run `yarn install`
4. After installation is complete, run `yarn start`
5. The app should automatically run in the development mode.
  If the app hasn't run, open [http://localhost:3000](http://localhost:3000) to view it in the browser.


##### Stack used to implement the server-side:

* NodeJS [NODE](https://nodejs.org/en/)
* Express.js [EXPRESS](https://expressjs.com/)
* SocketIO [SOCKET](https://socket.io/)
* MongoDB Atlas [MONGODB](https://www.mongodb.com/)
* mongoose

##### Stack used to implement the client-side:

* React

* Material-UI [MUI](https://material-ui.com/)

* Redux [Redux](https://redux.js.org/)
  * react-redux
  * redux-thunk
  * reselect-tools

* Axios [Axios](https://github.com/axios/axios)
  * axios
  * axios-mock-**adapter**

* socket.io-client
* react-router-dom